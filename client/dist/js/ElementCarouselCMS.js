(function($) {
  $.entwine('ss', function($){
      $('#Form_ItemEditForm_SlideType_Holder').entwine({
          onadd:function(){
              this.toggleFields();
          },
          onchange:function(){
              this.toggleFields();
          },
          toggleFields:function(){
              if(this.find('input:checked').length < 1){
                  this.find('input').first().attr('checked',true);
              }

              var value = this.find('input:checked').val(),
                  grids = this.closest('.tab').find('.grid-field');

              switch(value){
                  case 'Solnet\\Elements\\CarouselSlides\\CarouselSlide':
                      grids.hide();
                      $('#Form_ItemEditForm_HeaderSlides').show();
                      break;
                  case 'Solnet\\Elements\\CarouselSlides\\QuoteSlide':
                      grids.hide();
                      $('#Form_ItemEditForm_QuoteSlides').show();
                      break;
              }
          }
      });
  });
}(jQuery));

<?php

namespace Solnet\Elements\CarouselSlides;

use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Solnet\AdvancedLink\AdvancedLink;

/**
 * A single carousel slide, shown inside a Carousel element.
 */

class CarouselSlide extends DataObject
{
    use Configurable;

    private static $table_name = 'CarouselSlide';

    private static $db = [
        'TextAlign' => 'Varchar',
        'Title' => 'Varchar(255)',
        'Content' => 'Text',
    ];

    private static $has_one = [
        'Image' => Image::class,
        'CTALink' => AdvancedLink::class,
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'Content.Summary' => 'Content',
    ];

    /**
     * @config
     * Sets the types of alignment that are possible for this slide.
     * class output into template => Label shown in CMS
     */
    private static $text_align_options = [];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['BackgroundID', 'TextAlign', 'CTALinkID']);

        $textAlignOptions = $this->config()->text_align_options;
        if ($textAlignOptions && is_array($textAlignOptions) && count($textAlignOptions) > 0) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    DropdownField::create(
                        'TextAlign',
                        _t('Elements.SolnetCarousel_CarouselSlide_TextAlign_Title', 'Text alignment'),
                        $textAlignOptions
                    ),
                ]
            );
        }

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create(
                    'Title',
                    _t('Elements.SolnetCarousel_CarouselSlide_Title_Title', 'Title')
                ),
                $content = TextareaField::create(
                    'Content',
                    _t('Elements.SolnetCarousel_CarouselSlide_Content_Title', 'Content')
                ),
            ]
        );
        $content->setRows(5);

        if ($this->exists()) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    HasOneButtonField::create(
                        'CTALink',
                        _t('Elements.SolnetCarousel_Link_Title', 'Call To Action link'),
                        $this
                    ),
                ]
            );
            $fields->dataFieldByName('Image')->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Elements.SolnetCarousel_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        }

        return $fields;
    }
}

<?php

namespace Solnet\Elements\CarouselSlides;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class QuoteSlide extends DataObject
{
    private static $table_name = 'QuoteSlide';

    private static $db = [
        'Title' => 'Varchar(255)',
        'Quote' => 'HTMLText',
        'AuthorName' => 'Varchar(255)',
        'AuthorTitle' => 'Varchar(255)',
        'CTAText' => 'Varchar(100)',
        'AuthorFirst' => 'Boolean'
    ];

    private static $has_one = [
        'AuthorImage' => 'SilverStripe\Assets\Image',
        'Link' => 'SilverStripe\CMS\Model\SiteTree',
    ];

    private static $summary_fields = [
        'Quote.Summary' => 'Quote',
        'AuthorName' => 'Author'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(array('BackgroundID', 'AuthorImage'));

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create(
                    'Title',
                    _t('Elements.SolnetCarouselSlide_Title_Title', 'Title')
                ),
                HTMLEditorField::create(
                    'Quote',
                    _t('Elements.SolnetCarouselSlide_Quote_Title', 'Quote')
                )->setRows(3),
                TextField::create(
                    'AuthorName',
                    _t('Elements.SolnetCarouselSlide_AuthorName_Title', 'Author Name')
                ),
                TextField::create(
                    'AuthorTitle',
                    _t('Elements.SolnetCarouselSlide_AuthorTitle_Title', 'Author Title')
                ),
                CheckboxField::create(
                    'AuthorFirst',
                    _t('Elements.SolnetCarouselSlide_AuthorFirst_Title', 'Display Author First?')
                ),
                TextField::create(
                    'CTAText',
                    _t('Elements.SolnetCarouselSlide_CTAText_Title', 'CTA Text')
                ),
            ]
        );

        if ($this->exists()) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    $imageUpload = UploadField::create(
                        'AuthorImage',
                        _t('Elements.SolnetCarouselSlide_AuthorImage_Title', 'Author Image')
                    ),
                ]
            );
            $imageUpload->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Elements.SolnetCarouselSlide_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        }

        return $fields;
    }
}

<?php

namespace Solnet\Elements;

use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\View\Requirements;
use SilverStripe\ORM\ManyManyList;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementCarousel extends CustomBaseElement
{
    use Configurable;

    private static $table_name = 'ElementCarousel';
    private static $singular_name = 'Carousel';
    private static $plural_name = 'Carousels';
    private static $description = 'Carousel Module';

    private static $icon = 'font-icon-block-carousel';

    private static $db = [
        'SlideType' => 'Varchar'
    ];

    private static $many_many = [
        'HeaderSlides' => 'Solnet\Elements\CarouselSlides\CarouselSlide',
        'QuoteSlides' => 'Solnet\Elements\CarouselSlides\QuoteSlide'
    ];

    private static $many_many_extraFields = [
        'HeaderSlides' => array('SortOrder' => 'Int'),
        'QuoteSlides' => array('SortOrder' => 'Int')
    ];

    private static $cascade_duplicates = [
        'HeaderSlides',
        'QuoteSlides',
    ];
  
    public function getType()
    {
        return _t('Elements.SolnetCarousel_BlockType', 'Carousel');
    }

    public function getCMSFields()
    {
        Requirements::javascript('solnet/silverstripe-element-carousel:client/dist/js/ElementCarouselCMS.js');

        $fields = parent::getCMSFields();

        $fields->removeByName(array('SlideType', 'HeaderSlides', 'QuoteSlides'));

        $types = Config::inst()->get('Solnet\Elements\ElementCarousel', 'data_types');

        if ($this->exists() && is_array($types) && count($types) > 0) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    OptionsetField::create(
                        'SlideType',
                        _t('Elements.SolnetCarousel_SlideType_Title', 'Slide Type'),
                        $types,
                        array_keys($types)[0]
                    ),
                    GridField::create(
                        'HeaderSlides',
                        _t('Elements.SolnetCarousel_HeaderSlides_Title', 'Header Slides'),
                        $this->SortedHeaderSlides(),
                        GridFieldConfig_RelationEditor::create()->addComponent(
                            new GridFieldOrderableRows('SortOrder')
                        )
                    ),
                    GridField::create(
                        'QuoteSlides',
                        _t('Elements.SolnetCarousel_QuoteSlides_Title', 'Quote Slides'),
                        $this->SortedQuoteSlides(),
                        GridFieldConfig_RelationEditor::create()->addComponent(
                            new GridFieldOrderableRows('SortOrder')
                        )
                    )
                ]
            );
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Elements.SolnetCarousel_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        }

        return $fields;
    }

    public function SortedHeaderSlides(){
        return $this->HeaderSlides()->sort('SortOrder');
    }

    public function SortedQuoteSlides(){
        return $this->QuoteSlides()->sort('SortOrder');
    }

    /**
     * Ensure that after creating a duplicate of this element, the duplicate
     * contains copies of all many_many relations.
     *
     * Slides attached to the duplicate are linked to new copies
     * of the original's slides, so that editing slides on the new copy doesn't
     * affect slides on the original.
     *
     * @param ElementCarousel $original The object just cloned
     * @param boolean $doWrite whether or not to write the clone to the database
     * @param array|null $relations
     */
    public function onAfterDuplicate($original, $doWrite, $relations)
    {
        if (!is_array($relations)) {
            $relations = [];
        }
        foreach ($relations as $relationFieldName) {
            // Get list of items from original
            $source = $original->getManyManyComponents($relationFieldName);
            $dest = $this->getManyManyComponents($relationFieldName);
            // Remove the already-duplicated items; we're about to duplicate them again
            $dest->removeAll();
            // Get extrafield names (eg. copy the sort order)
            if ($source instanceof ManyManyList) {
                $extraFieldNames = $source->getExtraFields();
            } else {
                $extraFieldNames = [];
            }
            // Loop through items from original and add to copy
            // with matching extrafields
            foreach ($source as $sourceItem) {
                $clone = $sourceItem->duplicate($doWrite);
                $extraFields = [];
                foreach ($extraFieldNames as $fieldName => $fieldType) {
                    $extraFields[$fieldName] = $sourceItem->getField($fieldName);
                }
                $dest->add($clone, $extraFields);
            }
        }
    }
}
